#This tells the program we need the gem tts
require 'tts'

# Function that grabs the user input and plays it in English
# And then displays what was said in the command line.
# Allows us to easily just write "say()"
def say(word)
  s = "#{word}"
  repeatTimes = 1
  s.play("es", repeatTimes)
  puts
  puts "Succesfully Said: #{word}"
end

run = "yes"

while  run == "yes"
  #Loops unless the user does not want to enter something again
  # Prompt asking for the user to type in what they want the computer to say
  puts
  puts "What would you like to say?:"
  say(gets.to_s)
  puts
  puts "Would you like to say something else?"
  puts "yes / no"
  run = gets.chomp
end
# Says "Bye" and ends the program if the user does not want to say another word
puts
say("Bye")
puts "Bye"
# End of program